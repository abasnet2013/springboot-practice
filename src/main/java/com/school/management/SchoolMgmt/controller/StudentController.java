package com.school.management.SchoolMgmt.controller;

import com.school.management.SchoolMgmt.dto.InputRequest;
import com.school.management.SchoolMgmt.entity.Student;
import com.school.management.SchoolMgmt.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/student")
public class StudentController {

    @Autowired
    StudentService studentService;

    @PostMapping("/save")
    public Student saveStudent(@RequestBody Student s){
        Student stud =studentService.saveStudent(s);
        return stud;
    }
    @GetMapping("/getAll")
    public List<Student> getAllStudent(){
        List<Student> studentList = studentService.getAllStudent();
        return studentList;
    }

    @GetMapping("/getbyid/{id}")
    public Student StudentById(@PathVariable int id){
        Student studId =studentService.StudentById(id);
        return studId;
    }

    @DeleteMapping("/deletebyid/{id}")
    public void deleteStudentById(@PathVariable int id){
         studentService.deleteStudentById(id);
    }

    @DeleteMapping("/deleteall")
    public void deleteAll(){
        studentService.deleteAll();
    }

    @PutMapping ("/addstudent")
    public Student saveStudent( @RequestBody InputRequest<Student> request){
        return studentService.saveStudent(request);
    }

    @PostMapping("/updatestudent")
    public Student updateStudent( @RequestBody InputRequest<Student> request){
        return studentService.updateStudent(request);

    }


}
