package com.school.management.SchoolMgmt.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InputRequest<T> {

    @JsonProperty(value = "userId",required = true)
    private String loggedInUser;

    private T student;

    public InputRequest() {

    }

    public InputRequest(String loggedInUser, T student) {
        this.loggedInUser = loggedInUser;
        this.student = student;
    }

    public String getLoggedInUser() {
        return loggedInUser;
    }

    public void setLoggedInUser(String loggedInUser) {
        this.loggedInUser = loggedInUser;
    }



    public T getStudent() {
        return student;
    }

    public void setStudent(T student) {
        this.student = student;
    }


}
