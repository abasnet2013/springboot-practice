package com.school.management.SchoolMgmt.service;

import com.school.management.SchoolMgmt.dto.InputRequest;
import com.school.management.SchoolMgmt.entity.Student;
import com.school.management.SchoolMgmt.repo.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.Id;
import java.util.Calendar;
import java.util.List;

@Service
public class StudentService {


    @Autowired
    private StudentRepo studentRepo;

    public Student saveStudent(Student student){
        Student S = studentRepo.save(student);
        return S;
    }
    public List<Student> getAllStudent(){
        List<Student> studentList= studentRepo.findAll();
        return studentList;
    }
    public Student StudentById(int id){
        Student st = studentRepo.findById(id).get();
        return st;
    }
    public void deleteStudentById(int id){
        studentRepo.deleteById(id);
    }

    public void deleteAll(){
        studentRepo.deleteAll();
    }

    public Student saveStudent(InputRequest<Student> request){
        String currentUser = request.getLoggedInUser();
        Student student = request.getStudent();
        student.setCreateBy(currentUser);
        return studentRepo.save(student);
    }

    public Student updateStudent(InputRequest<Student> request){
        Student student =request.getStudent();
//        if(student!=null){
//            student.setAddress();
//            student.setLastModifiedBy(request.getLoggedInUser());
//            studentRepo.saveAndFlush(student);
//        }else{
//            throw new RuntimeException("Student not found with id : " + id);
//        }
        Student oldStudent = studentRepo.findById(student.getId()).orElse(null);
        oldStudent.setId(student.getId());
        oldStudent.setAddress(student.getAddress());

        return studentRepo.save(oldStudent);
    }

//    public String updateStudent(int id, InputRequest<Student> request){
//        Student student = studentRepo.findById(id).get();
//        return student;
//    }

}
