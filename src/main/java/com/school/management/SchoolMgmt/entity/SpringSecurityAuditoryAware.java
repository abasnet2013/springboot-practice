package com.school.management.SchoolMgmt.entity;

import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

public class SpringSecurityAuditoryAware implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.empty();
    }
}
